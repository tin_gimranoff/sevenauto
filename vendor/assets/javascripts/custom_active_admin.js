$(document).ready(function(){
	$('.custom legend').bind('click', function(){
		if($(".custom ol.choices-group li").css('display') == 'none')
			$(".custom ol.choices-group li").slideDown(500);
		else
			$(".custom ol.choices-group li").slideUp(500);
	});

	$("#car_mark").bind('change', function(){
		$.ajax({
                  type: 'POST',
                  url: '/ajax/get_models',
                  data: 'id='+$(this).val(),
                  dataType: 'json',
                  success: function(data){
                    var items = '';
                    $.each(data, function(i, item){
                        items = items + '<option value="'+item[0]+'">'+item[2]+'</option>';
                    });
                    $("#car_model").empty().append(items);
                  }
        });
	});
});
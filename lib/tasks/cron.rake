task :cron => :environment do
  shop_query = ShopQuery.includes(:car_mark).includes(:car_model).where('status = 0').limit(1)
  if shop_query.count == 0
  	puts "No queries....\n"
  	abort
  end
  spare_parts = SparePart.includes(:car_mark).where(:car_mark => { :id_car_mark => shop_query[0].car_mark_id }).where('email <> "" AND email IS NOT NULL')
  spare_parts.each do |s|
  	ShopQueryMailer.send_query_mail(shop_query[0].car_mark.name, shop_query[0].car_model.name, shop_query[0].year, shop_query[0].power, shop_query[0].size, shop_query[0].win, shop_query[0].name, shop_query[0].email, shop_query[0].phone, shop_query[0].description, s.email).deliver
  end
  shop_query[0].status = 1
  shop_query[0].save
  puts "All complite...\n"
  abort
end
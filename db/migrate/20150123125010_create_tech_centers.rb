class CreateTechCenters < ActiveRecord::Migration
  def change
    create_table :tech_centers do |t|
      t.has_attached_file :image
      t.string :name
      t.text :description
      t.string :phone
      t.string :email
      t.string :site
      t.string :address

      t.timestamps
    end
  end
end

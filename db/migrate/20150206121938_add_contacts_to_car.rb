class AddContactsToCar < ActiveRecord::Migration
  def change
    add_column :cars, :phone, :string
    add_column :cars, :city, :string
  end
end

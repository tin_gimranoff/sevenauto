class AddShopToUser < ActiveRecord::Migration
  def change
    add_column :users, :shop, :bool
  end
end

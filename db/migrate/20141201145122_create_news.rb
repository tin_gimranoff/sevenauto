class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :title
      t.text :intro
      t.string :alias
      t.has_attached_file :image
      t.boolean :week
      t.text :content

      t.timestamps
    end
  end
end

class CreateRequestRents < ActiveRecord::Migration
  def change
    create_table :request_rents do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.string :sq
      t.text :info

      t.timestamps
    end
  end
end

class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.integer :mark
      t.integer :model
      t.integer :status_new
      t.integer :status_access
      t.integer :price
      t.integer :year
      t.integer :mileage
      t.string :drive
      t.string :transmission
      t.string :steering_wheel
      t.integer :displacement
      t.integer :power
      t.string :body_type
      t.string :color
      t.integer :abs
      t.integer :bort_computer
      t.integer :car_condition
      t.integer :cruise_control
      t.integer :heated_mirrors
      t.integer :security_system
      t.integer :parktronic
      t.integer :power_steering
      t.integer :central_locking
      t.integer :electro_lifts
      t.integer :electro_mirrors
      t.has_attached_file :photo1
      t.has_attached_file :photo2
      t.has_attached_file :photo3
      t.integer :special

      t.timestamps
    end
  end
end

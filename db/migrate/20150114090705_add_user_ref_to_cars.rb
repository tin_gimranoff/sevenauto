class AddUserRefToCars < ActiveRecord::Migration
  def change
  	add_column :cars, :description, :text
  	add_attachment  :cars, :photo4
  	add_attachment  :cars, :photo5
  	add_column :cars, :remote_start, :integer
  	add_column :cars, :dtc, :integer
  	add_column :cars, :esc, :integer
  	add_column :cars, :airbag, :integer
    add_reference :cars, :user, index: true
  end
end

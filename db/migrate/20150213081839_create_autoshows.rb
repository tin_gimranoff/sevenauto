class CreateAutoshows < ActiveRecord::Migration
  def change
    create_table :autoshows do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.string :address
      t.string :site
      t.text :description
      t.has_attached_file :image
      t.references :user

      t.timestamps
    end
  end
end

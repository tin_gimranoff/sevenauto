class SpareParts < ActiveRecord::Migration
  def change
  	create_table :spare_parts do |t|
      t.has_attached_file :image
      t.string :name
      t.text :description
      t.string :phone
      t.string :email
      t.string :site
      t.string :address
      t.timestamps
    end
 
    create_table :car_mark_spare_parts, id: false do |t|
      t.belongs_to :spare_part, index: true
      t.belongs_to :car_mark, index: true
    end
  end
end

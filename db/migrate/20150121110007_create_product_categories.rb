class CreateProductCategories < ActiveRecord::Migration
  def change
    create_table :product_categories do |t|
      t.string :name

      t.timestamps
    end

    create_table :product_categories_spare_parts, id: false do |t|
      t.belongs_to :spare_part, index: true
      t.belongs_to :product_category, index: true
    end
  end
end

class RenameParamsToSettings < ActiveRecord::Migration
  def change
  	rename_table :params, :settings
  end
end

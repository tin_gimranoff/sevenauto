class AddParentToMenu < ActiveRecord::Migration
  def change
    add_column :menus, :parent, :integer
  end
end

class AddContactsToUser < ActiveRecord::Migration
  def change
  	add_column :users, :name, :string
  	add_column :users, :city, :string
  	add_column :users, :phone, :string
  	add_column :users, :addition_info, :string
  end
end

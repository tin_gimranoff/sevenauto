class CreateShopQueries < ActiveRecord::Migration
  def change
    create_table :shop_queries do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :win
      t.string :power
      t.string :size
      t.references :car_mark
      t.references :car_model
      t.integer :year
      t.text :description
      t.integer :status
      t.timestamps
    end
  end
end

class CreateWorkCategories < ActiveRecord::Migration
  def change
    create_table :work_categories do |t|
      t.string :name

      t.timestamps
    end

    create_table :tech_centers_work_categories, id: false do |t|
      t.belongs_to :tech_center, index: true
      t.belongs_to :work_category, index: true
    end
  end
end

class AddDescriptionToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :description, :string
  end
end

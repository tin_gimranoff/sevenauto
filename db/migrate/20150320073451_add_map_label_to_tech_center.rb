class AddMapLabelToTechCenter < ActiveRecord::Migration
  def change
    add_column :tech_centers, :map_label, :string
  end
end

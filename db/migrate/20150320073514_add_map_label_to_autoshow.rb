class AddMapLabelToAutoshow < ActiveRecord::Migration
  def change
    add_column :autoshows, :map_label, :string
  end
end

class AddMapLabelToSparePart < ActiveRecord::Migration
  def change
    add_column :spare_parts, :map_label, :string
  end
end

# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150320073514) do

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "autoshows", force: true do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.string   "address"
    t.string   "site"
    t.text     "description"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "map_label"
  end

  create_table "banners", force: true do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "label"
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "description"
  end

  create_table "car_characteristic", primary_key: "id_car_characteristic", force: true do |t|
    t.string  "name"
    t.integer "id_parent"
  end

  create_table "car_characteristic_value", primary_key: "id_car_characteristic_value", force: true do |t|
    t.string  "value"
    t.string  "unit"
    t.integer "id_car_characteristic"
    t.integer "id_car_modification"
  end

  add_index "car_characteristic_value", ["id_car_characteristic", "id_car_modification"], name: "id_characteristic", unique: true, using: :btree

  create_table "car_generation", primary_key: "id_car_generation", force: true do |t|
    t.string  "name",         null: false
    t.integer "id_car_model", null: false
    t.string  "year_begin"
    t.string  "year_end"
  end

  create_table "car_mark", primary_key: "id_car_mark", force: true do |t|
    t.string "name", null: false
  end

  create_table "car_mark_spare_parts", id: false, force: true do |t|
    t.integer "spare_part_id"
    t.integer "car_mark_id"
  end

  add_index "car_mark_spare_parts", ["car_mark_id"], name: "index_rel_mark_spare_parts_on_car_mark_id", using: :btree
  add_index "car_mark_spare_parts", ["spare_part_id"], name: "index_rel_mark_spare_parts_on_spare_part_id", using: :btree

  create_table "car_model", primary_key: "id_car_model", force: true do |t|
    t.integer "id_car_mark", null: false
    t.string  "name",        null: false
  end

  add_index "car_model", ["name"], name: "name", using: :btree

  create_table "car_modification", primary_key: "id_car_modification", force: true do |t|
    t.integer "id_car_serie",          null: false
    t.integer "id_car_model",          null: false
    t.string  "name",                  null: false
    t.integer "start_production_year"
    t.integer "end_production_year"
  end

  create_table "car_serie", primary_key: "id_car_serie", force: true do |t|
    t.integer "id_car_model",      null: false
    t.string  "name",              null: false
    t.integer "id_car_generation"
  end

  add_index "car_serie", ["name"], name: "name", using: :btree

  create_table "cars", force: true do |t|
    t.integer  "mark"
    t.integer  "model"
    t.integer  "status_new"
    t.integer  "status_access"
    t.integer  "price"
    t.integer  "year"
    t.integer  "mileage"
    t.string   "drive"
    t.string   "transmission"
    t.string   "steering_wheel"
    t.integer  "displacement"
    t.integer  "power"
    t.string   "body_type"
    t.string   "color"
    t.integer  "abs"
    t.integer  "bort_computer"
    t.integer  "car_condition"
    t.integer  "cruise_control"
    t.integer  "heated_mirrors"
    t.integer  "security_system"
    t.integer  "parktronic"
    t.integer  "power_steering"
    t.integer  "central_locking"
    t.integer  "electro_lifts"
    t.integer  "electro_mirrors"
    t.string   "photo1_file_name"
    t.string   "photo1_content_type"
    t.integer  "photo1_file_size"
    t.datetime "photo1_updated_at"
    t.string   "photo2_file_name"
    t.string   "photo2_content_type"
    t.integer  "photo2_file_size"
    t.datetime "photo2_updated_at"
    t.string   "photo3_file_name"
    t.string   "photo3_content_type"
    t.integer  "photo3_file_size"
    t.datetime "photo3_updated_at"
    t.integer  "special"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description"
    t.string   "photo4_file_name"
    t.string   "photo4_content_type"
    t.integer  "photo4_file_size"
    t.datetime "photo4_updated_at"
    t.string   "photo5_file_name"
    t.string   "photo5_content_type"
    t.integer  "photo5_file_size"
    t.datetime "photo5_updated_at"
    t.integer  "remote_start"
    t.integer  "dtc"
    t.integer  "esc"
    t.integer  "airbag"
    t.integer  "user_id"
    t.string   "phone"
    t.string   "city"
  end

  add_index "cars", ["user_id"], name: "index_cars_on_user_id", using: :btree

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "menus", force: true do |t|
    t.string   "name"
    t.string   "url"
    t.integer  "ordernum"
    t.integer  "hide"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "parent"
  end

  create_table "news", force: true do |t|
    t.string   "title"
    t.text     "intro"
    t.string   "alias"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.boolean  "week"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "product_categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "product_categories_spare_parts", id: false, force: true do |t|
    t.integer "spare_part_id"
    t.integer "product_category_id"
  end

  add_index "product_categories_spare_parts", ["product_category_id"], name: "index_product_categories_spare_parts_on_product_category_id", using: :btree
  add_index "product_categories_spare_parts", ["spare_part_id"], name: "index_product_categories_spare_parts_on_spare_part_id", using: :btree

  create_table "request_rents", force: true do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.string   "sq"
    t.text     "info"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "seos", force: true do |t|
    t.string   "url"
    t.string   "title"
    t.text     "keywords"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "settings", force: true do |t|
    t.string   "param_name"
    t.string   "param_key"
    t.string   "param_value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "shop_queries", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.string   "win"
    t.string   "power"
    t.string   "size"
    t.integer  "car_mark_id"
    t.integer  "car_model_id"
    t.integer  "year"
    t.text     "description"
    t.integer  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "spare_parts", force: true do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "name"
    t.text     "description"
    t.string   "phone"
    t.string   "email"
    t.string   "site"
    t.string   "address"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "map_label"
  end

  create_table "tech_centers", force: true do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "name"
    t.text     "description"
    t.string   "phone"
    t.string   "email"
    t.string   "site"
    t.string   "address"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "map_label"
  end

  create_table "tech_centers_work_categories", id: false, force: true do |t|
    t.integer "tech_center_id"
    t.integer "work_category_id"
  end

  add_index "tech_centers_work_categories", ["tech_center_id"], name: "index_tech_centers_work_categories_on_tech_center_id", using: :btree
  add_index "tech_centers_work_categories", ["work_category_id"], name: "index_tech_centers_work_categories_on_work_category_id", using: :btree

  create_table "textpages", force: true do |t|
    t.string   "name"
    t.string   "slug"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin"
    t.string   "name"
    t.string   "city"
    t.string   "phone"
    t.string   "addition_info"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.boolean  "shop"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "work_categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end

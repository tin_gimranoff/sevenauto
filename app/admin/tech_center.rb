#coding: utf-8
ActiveAdmin.register TechCenter do

  menu parent: "Технические центры", label: "Технические центры"
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :image, :name, :description, :phone, :email, :site, :address, :map_label,
    work_category: [:id, :name],
    work_category_ids: []

  remove_filter :work_category

  index do 
    selectable_column 
    id_column 
    column :name 
    column :description 
    column :phone 
    column :email 
    column :site 
    column :address 
    column :map_label
  actions 
  end

  form(:html => {:multipart => true}) do |f| 
  f.inputs "Новый технический центр" do 
    f.input :name
    f.input :description, as: :ckeditor
    f.input :phone
    f.input :email
    f.input :site 
    f.input :address 
    f.input :map_label
    f.input :image, :required => true, :as => :file
  end

  f.inputs "Категории работ" do
    f.input :work_categories, as: :check_boxes, :multiple => true, label: ''
  end
   
  f.actions 
  end


end

#coding: utf-8
ActiveAdmin.register Settings do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  menu label: "Парметры"
  permit_params :param_name, :param_key, :param_value
  index do
    selectable_column
    id_column
    column :param_name
    column :param_key
    column :param_value
    column :created_at
    column :updated_at
    actions
  end

    form(:html => {:multipart => true}) do |f|
      f.inputs "Новый парметр" do
        f.input :param_name
        f.input :param_key
        f.input :param_value
      end
      f.actions
  end


end

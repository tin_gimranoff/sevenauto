ActiveAdmin.register Autoshow do

  menu label: "Автосалоны"

  permit_params :name, :description, :phone, :email, :site, :address, :image, :user_id, :map_label

  index do 
    selectable_column 
    id_column 
    column :name 
    column :phone 
    column :email 
    column :site 
    column :address 
    column :map_label
    column :user_id do |u|
      User.find(u.user_id).email
    end
  actions 
  end


  form(:html => {:multipart => true}) do |f| 
  f.inputs "Новый автосалон" do 
    f.input :name
    f.input :description, as: :ckeditor
    f.input :phone
    f.input :email
    f.input :site 
    f.input :address 
    f.input :map_label
    f.input :image, :required => true, :as => :file
    f.input :user_id, as: :select, collection: User.where(shop: true) 
  end
  f.actions 
  end
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end

#coding: utf-8
ActiveAdmin.register User do

  permit_params :email, :name, :city, :phone, :addition_info, :image, :admin, :shop

  index do
    selectable_column
    id_column
    column :email
    column :current_sign_in_at
    column :last_sign_in_at
    column :sign_in_count
    column :name
    column :city
    column :phone
    column :shop do |u|
      u.admin
      if u.shop == true
        "Да"
      else
        "Нет"
      end
    end
    column :admin do |u|
      u.admin
      if u.admin == true
        "Да"
      else
        "Нет"
      end
    end
    actions
end

filter :email

  form do |f|
    f.inputs "Новый пользователь" do
    f.input :email
    f.input :name
    f.input :city
    f.input :phone
    f.input :addition_info
    f.input :image, :required => false, :as => :file
    f.input :shop, as: :select, collection: [["Да", true], ["Нет", false]]
    f.input :admin, as: :select, collection: [["Да", true], ["Нет", false]]
  end

  f.actions

end
end
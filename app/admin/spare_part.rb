#coding: utf-8
ActiveAdmin.register SparePart do

  menu parent: "Магазины", label: "Магазины"
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :image, :name, :description, :phone, :email, :site, :address, :map_label,
    car_mark_attributes: [:id_car_mark, :name], product_category: [:id, :name],
    car_mark_ids:[], product_category_ids: []

  remove_filter :car_mark, :poroduct_category

  index do 
    selectable_column 
    id_column 
    column :name 
    column :description 
    column :phone 
    column :email 
    column :site 
    column :address 
    column :map_label
  actions 
  end

  form(:html => {:multipart => true}) do |f| 
  f.inputs "Новый магазин" do 
    f.input :name
    f.input :description, as: :ckeditor
    f.input :phone
    f.input :email
    f.input :site 
    f.input :address 
    f.input :map_label
    f.input :image, :required => true, :as => :file
  end

  f.inputs "Категории товаров (кликните что бы показать список)" do
    f.input :product_categories, as: :check_boxes, :multiple => true, label: ''
  end

  #тут нужна форма с checkbox  для всех sidebars. Выбранные sidebars должны передавать в связную таблицу id page + выбранные для нее sidebars 
  f.inputs "Марки машин (кликните что бы показать список)", :class => 'custom' do 
    f.input :car_mark, as: :check_boxes, :multiple => true, label: '', collection: CarMark.order('name ASC')
  end
   
  f.actions 
  end
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end



end


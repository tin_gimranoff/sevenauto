#coding: utf-8
ActiveAdmin.register ProductCategory do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  menu parent: "Магазины"
  permit_params :name
  index do
    selectable_column
    id_column
    column :name
    column :created_at
    column :updated_at
    actions
  end

  form(:html => {:multipart => true}) do |f|
    f.inputs "Новая категория" do
      f.input :name
    end
    f.actions
  end
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end

#coding: utf-8
ActiveAdmin.register Car do

  menu label: "Легковые авто"
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :mark, :model, :status_new, :status_access, :price, :year, :mileage, :drive, :transmission, :steering_wheel, :displacement, :power, :body_type, :color, :abs, :bort_computer, :car_condition, :cruise_control, :heated_mirrors, :security_system, :parktronic, :power_steering, :central_locking, :electro_lifts, :electro_mirrors, :photo1, :photo2, :photo3, :special, :photo4, :photo5, :description, :dtc, :esc, :remote_start, :airbag, :user_id, :phone, :city
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  remove_filter :car_mark
  remove_filter :car_model

  index do
    selectable_column
    id_column
    column :mark do |m|
      CarMark.find(m.mark).name
    end
    column :model do |m|
      CarModel.find(m.model).name
    end
    column :created_at
    column :updated_at
    actions
  end

  form(:html => {:multipart => true}) do |f|
      f.inputs "Новое авто" do
        f.input :mark, as: :select, collection: CarMark.all
        f.input :model, as: :select, collection: CarModel.all
        f.input :phone
        f.input :city
        f.input :status_new, as: :select, collection: [["Да", 1], ["Нет", 0]]
        f.input :status_access, as: :select, collection: [["Да", 1], ["Нет", 0]]
        f.input :price
        f.input :year
        f.input :mileage
        f.input :drive, as: :select, collection: [["Передний", "Передний"], ["Задний", "Задний"], ["4x4", "4x4"]]
        f.input :transmission, as: :select, collection: [["Механическа", "Механическа"], ["Автоматическая", "Автоматическая"]]
        f.input :steering_wheel, as: :select, collection: [["Левый", "Левый"], ["Правый", "Правый"]] 
        f.input :displacement
        f.input :power
        f.input :body_type, as: :select, collection: [["Седан", "Седан"], ["Хэтчбек", "Хэтчбек"], ["Универсал","Универсал"], ["Кросовер","Кросовер"], ["Внедорожник","Внедорожник"], ["Лифтбек","Лифтбек"], ["Купе","Купе"], ["Пикап","Пикап"], ["Кабриолет","Кабриолет"], ["Лимузин","Лимузин"], ["Минивен","Минивен"], ["Фургон","Фургон"]]
        f.input :color, as: :select, collection: [["Черный", "Черный"], ["Коричневый","Коричневый"], ["Белый","Белый"], ["Бежевый","Бежевый"], ["Серый","Серый"], ["Красный","Красный"], ["Оранжевый","Оранжевый"], ["Розовый","Розовый"], ["Пурпурный","Пурпурный"], ["Желтый","Желтый"], ["Зеленый","Зеленый"], ["Голубой","Голубой"], ["Синий","Синий"], ["Фиолетовый","Фиолетовый"]]
        f.input :abs, as: :select, collection: [["Да", 1], ["Нет", 0]]
        f.input :bort_computer, as: :select, collection: [["Да", 1], ["Нет", 0]]
        f.input :car_condition, as: :select, collection: [["Да", 1], ["Нет", 0]]
        f.input :cruise_control, as: :select, collection: [["Да", 1], ["Нет", 0]]
        f.input :heated_mirrors, as: :select, collection: [["Да", 1], ["Нет", 0]]
        f.input :security_system, as: :select, collection: [["Да", 1], ["Нет", 0]]
        f.input :parktronic, as: :select, collection: [["Да", 1], ["Нет", 0]]
        f.input :power_steering, as: :select, collection: [["Да", 1], ["Нет", 0]]
        f.input :central_locking, as: :select, collection: [["Да", 1], ["Нет", 0]]
        f.input :electro_lifts, as: :select, collection: [["Да", 1], ["Нет", 0]]
        f.input :electro_mirrors, as: :select, collection: [["Да", 1], ["Нет", 0]]
        f.input :remote_start, as: :select, collection: [["Да", 1], ["Нет", 0]]
        f.input :dtc, as: :select, collection: [["Да", 1], ["Нет", 0]]
        f.input :esc, as: :select, collection: [["Да", 1], ["Нет", 0]]
        f.input :airbag, as: :select, collection: [["Да", 1], ["Нет", 0]]
        f.input :special, as: :select, collection: [["Да", 1], ["Нет", 0]]
        f.input :description, :as => :ckeditor
        f.input :photo1, :required => false, :as => :file
        f.input :photo2, :required => false, :as => :file
        f.input :photo3, :required => false, :as => :file
        f.input :photo4, :required => false, :as => :file
        f.input :photo5, :required => false, :as => :file
        f.input :user_id, as: :select, collection: User.all
      end
      f.actions
  end

end

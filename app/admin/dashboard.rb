#coding: utf-8
ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    div class: "blank_slate_container", id: "dashboard_default_message" do

    end

    # Here is an example of a simple dashboard with columns and panels.
    #
    columns do
       column do
         panel "Последний объявления" do
           ul do
             Car.order("id DESC").limit(5).map do |car|
               li link_to(CarMark.find(car.mark).name+' '+CarModel.find(car.model).name+' '+car.power.to_s+' л.с.,'+car.year.to_s+' г.в.,'+number_with_delimiter(car.mileage).to_s+' км,'+number_to_currency(car.price, locale: :ru, precision: 0).to_s, admin_car_path(car))
             end
           end
         end
       end

        column do
         panel "Последние магазины" do
           ul do
             SparePart.order("id DESC").limit(5).map do |s|
               li link_to(s.name, admin_spare_part_path(s))
             end
           end
         end
        end
        column do
         panel "Последние тех. центры" do
           ul do
             TechCenter.order("id DESC").limit(5).map do |s|
               li link_to(s.name, admin_spare_part_path(s))
             end
           end
         end
        end
    end

  end # content
end

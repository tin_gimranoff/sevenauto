#coding: utf-8
ActiveAdmin.register News do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  menu parent: "Структура"
  permit_params :title, :intro, :alias, :content, :image, :week
  index do
    selectable_column
    id_column
    column :title
    column :intro
    column :alias
    column :created_at
    column :updated_at
    actions
  end

    form(:html => {:multipart => true}) do |f|
      f.inputs "Новая новость" do
        f.input :title
        f.input :intro, :as => :ckeditor
        f.input :alias
        f.input :content,:as => :ckeditor
        f.input :image, :required => false, :as => :file
        f.input :week
      end
      f.actions
  end


end

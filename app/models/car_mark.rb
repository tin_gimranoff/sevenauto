class CarMark < ActiveRecord::Base
	self.table_name = 'car_mark'
	self.primary_key = 'id_car_mark'

	has_and_belongs_to_many :spare_parts

	has_many :shop_queries
	has_many :car, :class_name => 'Car', :foreign_key => 'mark'
end
class Settings < ActiveRecord::Base
	validates :param_name, :param_key, :param_value, presence: true

	def self.get_param_value(param_name)
		return self.where('param_key = ?', param_name)[0].param_value
	end
end

class Car < ActiveRecord::Base
	after_initialize :default_values

	belongs_to :user
	belongs_to :car_mark, :foreign_key => 'mark'
	belongs_to :car_model, :foreign_key => 'model'

	has_attached_file :photo1, :styles => { :thumb => "120x90>", :details_thumb => "76x57>", :details => "400x300>" }, :path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", :url => "/:class/:attachment/:id/:style_:basename.:extension"
	has_attached_file :photo2, :styles => { :thumb => "120x90>", :details_thumb => "76x57>", :details => "400x300>" }, :path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", :url => "/:class/:attachment/:id/:style_:basename.:extension"
	has_attached_file :photo3, :styles => { :thumb => "120x90>", :details_thumb => "76x57>", :details => "400x300>" }, :path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", :url => "/:class/:attachment/:id/:style_:basename.:extension"
	has_attached_file :photo4, :styles => { :thumb => "120x90>", :details_thumb => "76x57>", :details => "400x300>" }, :path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", :url => "/:class/:attachment/:id/:style_:basename.:extension"
	has_attached_file :photo5, :styles => { :thumb => "120x90>", :details_thumb => "76x57>", :details => "400x300>" }, :path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", :url => "/:class/:attachment/:id/:style_:basename.:extension"

	validates_attachment_content_type :photo1, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
	validates_attachment_content_type :photo2, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
	validates_attachment_content_type :photo3, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
	validates_attachment_content_type :photo4, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
	validates_attachment_content_type :photo5, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

	validates :mark, :model, :mileage, presence: true

	class_attribute :output_params

	def self.get_marks 
		query = "SELECT * FROM `car_mark`"
		connection = ActiveRecord::Base.connection
		result = connection.execute(query) 
		return result
	end

	def self.get_models(id) 
		query = "SELECT * FROM `car_model` WHERE id_car_mark = "+id
		connection = ActiveRecord::Base.connection
		result = connection.execute(query) 
		return result
	end

    def default_values
    	self.output_params = ['abs', 'bort_computer', 'car_condition', 'cruise_control', 'heated_mirrors', 'security_system', 'parktronic', 'power_steering', 'central_locking', 'electro_lifts', 'electro_mirrors', 'remote_start', 'dtc', 'airbag', 'esc']
    end
end

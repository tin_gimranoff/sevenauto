class News < ActiveRecord::Base

	has_attached_file :image, :styles => { :thumb => "198x144>", :inner => "80x52>" }, :path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", :url => "/:class/:attachment/:id/:style_:basename.:extension"

	validates :title, :intro, :alias, :content, presence: true

	validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end

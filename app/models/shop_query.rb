class ShopQuery < ActiveRecord::Base
	belongs_to :car_mark
	belongs_to :car_model

	validates :car_mark_id, :car_model_id, :email, :win, presence: true
	validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }
end

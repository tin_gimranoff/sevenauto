class CarModel < ActiveRecord::Base
	self.table_name = 'car_model'
	self.primary_key = 'id_car_model'

	has_many :shop_queries

	has_many :car, :class_name => 'Car', :foreign_key => 'model'
end
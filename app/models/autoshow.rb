class Autoshow < ActiveRecord::Base
	belongs_to :user

	has_attached_file :image, :styles => { :thumb => "117x117>", :thumb_details => "200x200>" }, :path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", :url => "/:class/:attachment/:id/:style_:basename.:extension"

	validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

	validates :name, :description, :phone, :address, :user_id,  presence: true
	validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }
end

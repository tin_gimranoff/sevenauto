class WorkCategory < ActiveRecord::Base
  has_and_belongs_to_many :tech_centers

  validates :name, presence: true
end

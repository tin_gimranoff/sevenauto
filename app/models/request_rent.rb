class RequestRent < ActiveRecord::Base
	validates :phone, presence: true

	validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }

	validates :sq, numericality: true
end

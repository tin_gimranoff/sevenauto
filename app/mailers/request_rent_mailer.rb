#coding: utf-8
class RequestRentMailer < ActionMailer::Base
  default from: "robot@7auto.pro"

  def send_rent_mail(name, phone, email, sq, info)
  	@name = name
  	@phone = phone 
  	@email = email 
  	@sq = sq 
  	@info = info 
  	mail(to: 'gimranov.valentin@yandex.ru', subject: 'Арендовать павильон') do |format|
      format.text { render "send_rent_mail" }
    end
  end
end

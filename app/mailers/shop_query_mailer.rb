#coding: utf-8
class ShopQueryMailer < ActionMailer::Base
  default from: "robot@7auto.pro"

  def send_query_mail(mark, model, year, power, size, win, name, email, phone, description, to_email)
  	@mark = mark
  	@model = model 
  	@year = year 
  	@power = power 
  	@size = size 
  	@win = win 
  	@name = name
  	@email = email
  	@phone = phone
  	@description = description
  	mail(to: to_email, subject: 'Найти запчасть') do |format|
      format.text { render "send_query_mail" }
    end
  end
end

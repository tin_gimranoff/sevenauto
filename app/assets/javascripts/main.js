var current_slider_image_sell = 0;
var current_slider_image_hardware = 0;
var current_slider_image_service = 0;
var suffix;
$(document).ready(function(){
  setTimeout(setInterval(function() { ch_slider('sell-auto', current_slider_image_sell); }, 6000), 4000);
  setTimeout(setInterval(function() { ch_slider('hardware-auto', current_slider_image_hardware) }, 9000), 3000);
  setTimeout(setInterval(function() { ch_slider('service-auto', current_slider_image_service) }, 12000), 6000);
	ymaps.ready(init);
    var myMap, 
    myPlacemark;
        function init(){ 
            myMap = new ymaps.Map("map", {
                center: [55.1190,36.6364],
                zoom: 7,
                controls: ['smallMapDefaultSet'],
            }); 
            
            myPlacemark = new ymaps.Placemark([55.119262,36.637831], {
                hintContent: 'Автотехцетр "7авто"',
                balloonContent: ''
            });
            
            myMap.geoObjects.add(myPlacemark);
        };
        //Слайдеры на главной
        //Показать-свернуть карту
        $("#show-map-btn, .print-scheme-center a").bind('click', function(){
            if($("#map").css('height') == '90px')
            {
                $("#map").animate({
                    height: 500,
                }, 500, function() {
                    $("#show-map-btn").attr('src', '/assets/close-map-btn.png');
                    myMap.destroy();
                    init();
                    $('html, body').animate({scrollTop: $('#map').offset().top}, 500);
                });
            } else {
                $("#map").animate({
                    height: 90,
                }, 500, function() {
                    $("#show-map-btn").attr('src', '/assets/show-map-btn.png');
                    myMap.destroy();
                    init();
                });  
            }
            return false;
        });
        //Показывать-скрыть окно селекта
        $(".select").bind('click', function(){
            if($("#"+$(this).attr('data-window')+"-select").css('display') == 'none')
                $("#"+$(this).attr('data-window')+"-select").css('display', 'block');
            else
                $("#"+$(this).attr('data-window')+"-select").css('display', 'none');
        });
        //Выбор значения из окна селекта
        $(".select-window-value").bind('click', function(){
            change_ajax_select_value(this);
            if($(this).attr('data-field') == 'mark' || $(this).attr('data-field') == 'mark-hardware') {
                if($(this).attr('data-field') == 'mark-hardware')
                  suffix = '-hardware';
                else
                  suffix = '';
                $.ajax({
                  type: 'POST',
                  url: '/ajax/get_models',
                  data: 'id='+$(this).attr('data-id'),
                  dataType: 'json',
                  success: function(data){
                    var items = '';
                    $.each(data, function(i, item){
                        items = items + '<a href="#" data-field="model'+suffix+'" data-id="'+item[0]+'" data-name="'+item[2]+'" class="select-window-value" onclick="change_ajax_select_value(this); return false;">'+item[2]+'</a>';
                    });
                    $("#model"+suffix+"-select").empty().append(items);
                    $("input[name='car[model"+suffix+"]']").attr('value', '');
                    $(".select[data-window=model"+suffix+"] .select-value").empty().append('--');
                  }
                });
            }
            return false;
        });
        //Выбор типа кузова
        $(".form-type li").bind('click', function(){
            $(".form-type li img").css('border', '1px dotted #dddddd');
            $($(this).children()[0]).css('border', '3px solid #a3cff7');
            $($(this).children()[0]).css('border-radius', '3px');
            $("input[name='car[body_type]']").attr('value', $(this).attr('data-type'));
        });
        //Выбор типа автомобля (новый, с пробегом)
        $(".type li").bind('click', function(){
            $(".type li").removeClass('active');
            $(this).addClass('active');
            $("#car_status_new").attr('value', $(this).attr('data-value'));
        });
        //Выбор типа комментировани (FB, VK)
        $(".comment_type li").bind('click', function(){
            $(".type li").removeClass('active');
            $(this).addClass('active');
            $(".social_block_comments").css('display', 'none');
            $(".user_edit_type").css('display', 'none');
            $("#"+$(this).attr('data-value')).css('display', 'block');
        });
        //Закрывать всплывающее окно
        $("#success_popup .popup_btn, .close-form").bind('click', function(){
            $(this).parent().hide();
            $("#overlay").hide();
            return false;
        });
        //Смена изображения по клику на превью
        $(".preview-image").bind('click', function(){
            var original_image = $(this).parent().attr('href');
            $(".original-image").attr('src', original_image);
            return false;
        });
        //Показать телефон
        $(".advert_info_phone").bind('click', function(){
          $(this).empty().append($(this).attr('data-value'));
        });
        //Сабмит формы поиска запчастей
        $(".search-harware-field form a").bind('click', function(){
          $($(this).parent()).submit();
        });
        //Найти запчасть
        $(".find-item").bind('click', function(){
            var car_mark_id = $("#car_mark_hardware_id").attr('value');
            var car_model_id = $("#car_model_hardware_id").attr('value');
            var car_year = $("#car_year_hardware").attr('value');
            if(car_mark_id == '' || car_model_id == '')
                showSuccessForm(518, 180, 'Вы не выбрали марку и модель', 'ОШИБКА В ЗАПРОСЕ')
            else
            {
              $("#shop_query_car_mark_id").attr('value', car_mark_id);
              $("#shop_query_car_model_id").attr('value', car_model_id);
              $("#shop_query_year").attr('value', car_year);
              showForm('hardware_popup');
            }
        });
        //Сабмит формы "Найти запчасть"
        $("#hardware_popup .popup_btn").bind('click', function(){
            $("#new_shop_query").submit();
        });

        //Арендовать авильон
        $(".rent-btn").bind('click', function(){
          showForm('rent_popup');
        });
        //Сабмит формы арендовать павильнон
        $("#rent_popup .popup_btn").bind('click', function(){
            $("#new_request_rent").submit();
        });
        //Раскрыть весь список с марками
        $(".car-marks .show-all").bind('click', function(){
          var El = $(".car-marks ul");
          if(El.height() == 20)
          {
             autoHeightAnimate(El, 500);
             $(this).empty().append('Свернуть');
          }
          else
          {
            El.animate({
              height: 20
            }, 500);
            $(this).empty().append('Показать все');
          }
          return false;
        });
});

function change_ajax_select_value(item) {
    $(".select[data-window="+$(item).attr('data-field')+"] .select-value").empty().append($(item).text());
    $("input[name='car["+$(item).attr('data-field')+"]']").attr('value', $(item).attr('data-id'));
    $(item).parent().css('display', 'none');
}

function  getPageSize(){
           var xScroll, yScroll;

           if (window.innerHeight && window.scrollMaxY) {
                   xScroll = document.body.scrollWidth;
                   yScroll = window.innerHeight + window.scrollMaxY;
           } else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
                   xScroll = document.body.scrollWidth;
                   yScroll = document.body.scrollHeight;
           } else if (document.documentElement && document.documentElement.scrollHeight > document.documentElement.offsetHeight){ // Explorer 6 strict mode
                   xScroll = document.documentElement.scrollWidth;
                   yScroll = document.documentElement.scrollHeight;
           } else { // Explorer Mac...would also work in Mozilla and Safari
                   xScroll = document.body.offsetWidth;
                   yScroll = document.body.offsetHeight;
           }

           var windowWidth, windowHeight;
           if (self.innerHeight) { // all except Explorer
                   windowWidth = self.innerWidth;
                   windowHeight = self.innerHeight;
           } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
                   windowWidth = document.documentElement.clientWidth;
                   windowHeight = document.documentElement.clientHeight;
           } else if (document.body) { // other Explorers
                   windowWidth = document.body.clientWidth;
                   windowHeight = document.body.clientHeight;
           }

           // for small pages with total height less then height of the viewport
           if(yScroll < windowHeight){
                   pageHeight = windowHeight;
           } else {
                   pageHeight = yScroll;
           }

           // for small pages with total width less then width of the viewport
           if(xScroll < windowWidth){
                   pageWidth = windowWidth;
           } else {
                   pageWidth = xScroll;
           }

           return [pageWidth,pageHeight,windowWidth,windowHeight];
}

function autoHeightAnimate(element, time){
    var curHeight = element.height(), // Get Default Height
        autoHeight = element.css('height', 'auto').height(); // Get Auto Height
        element.height(curHeight); // Reset to Default Height
        element.stop().animate({ height: autoHeight }, parseInt(time)); // Animate to Auto Height
}

function showSuccessForm(width, height, content, header)
{
    $("#success_popup").css('top', getPageSize()[3]/2-(height/2));
    $("#success_popup").css('left', getPageSize()[2]/2-(width/2));
    $("#success_popup #popup_content").empty().append(content);
    $("#success_popup #popup_header").empty().append(header);
    $("#overlay").show();
    $("#success_popup").show(500);
}

function showForm(type) {
    $("#"+type).css('height', 'auto');
      console.log($("#"+type).height());
    $("#"+type).css('height', $("#"+type).height()+100+'px');
    $("#"+type).css('top', getPageSize()[3]/2-($("#"+type).height()/2));
    $("#"+type).css('left', getPageSize()[2]/2-($("#"+type).width()/2));
    $("#overlay").show();
    $("#"+type).show(500);
}

function ch_slider(type_slider, current_slider_image) {
  var banners = $(".banner-slider[rel="+type_slider+"]");
  $(banners[current_slider_image]).fadeOut(500, function(){
      $($("."+type_slider)[current_slider_image]).removeClass('active');

      if(current_slider_image+1 == banners.size())
        current_slider_image = 0;
      else
        current_slider_image = current_slider_image + 1;
      $(banners[current_slider_image]).fadeIn(500);
      $($("."+type_slider)[current_slider_image]).addClass('active');

      if(type_slider == 'sell-auto')
          current_slider_image_sell = current_slider_image
      if(type_slider == 'hardware-auto')
          current_slider_image_hardware = current_slider_image
      if(type_slider == 'service-auto')
          current_slider_image_service = current_slider_image
  });

}

class TechcenterController < ApplicationController

	def techcenters
		@categories = WorkCategory.all
		if !params[:category].blank?
			@techcenters = TechCenter.includes(:work_categories).where(:work_categories => {:id => params[:category]})
		else
			@techcenters = TechCenter.all
		end
	end

	def techcenter_details 
		@techcenter = TechCenter.includes(:work_categories).find(params[:id]) rescue nil
		if @techcenter.nil?
			redirect_to '/404'
		end
	end

end
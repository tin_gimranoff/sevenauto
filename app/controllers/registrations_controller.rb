class RegistrationsController < Devise::RegistrationsController

  before_action :is_logged, only: [:advert]

  def create
    build_resource(registration_params)

    if resource.save
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_navigational_format?
        sign_up(resource_name, resource)
        respond_with resource, :location => after_sign_up_path_for(resource)
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_navigational_format?
        respond_with resource, :location => after_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      respond_with resource
    end
  end 

  def advert
    if request.delete?
      Car.find(params[:id]).delete()
      redirect_to :back, :notice => "Удалениие успешно удалено" 
      return
    end
    @car = Car.find(params[:id]) rescue nil
    if @car.nil?
      redirect_to '/404'
    end
    if params[:car]
      params.permit!
      if @car.update_attributes(params[:car])
        @status_success = 1
      end
    end
  end 

  private

  def registration_params
    params.require(:user).permit(:email, :name, :city, :phone, 
      :addition_info, :password, :password_confirmation)
  end

  def account_update_params
    params.require(:user).permit(:email, :name, :city, :phone, 
      :addition_info, :password, :password_confirmation, :image, :current_password)
  end

  def is_logged
    if user_signed_in?
      true
    else
      redirect_to '/users/sign_in'
    end
  end

end
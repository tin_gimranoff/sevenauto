class SiteController < ApplicationController

	before_filter :construct

	def index
		@marks = CarMark.select('`car_mark`.`id_car_mark`, `car_mark`.`name`, count(`car_mark`.`id_car_mark`) as `count_cars`')
				 .joins('JOIN `cars` ON `cars`.`mark`=`car_mark`.`id_car_mark`')
				 .group('`car_mark`.`id_car_mark`')
				 .order('count(`car_mark`.`id_car_mark`) DESC').limit(5)
		if params[:shop_query]
			params.permit!
			@shop_query = ShopQuery.new(params[:shop_query])
			if @shop_query.valid?
				@shop_query.status = 0
				@shop_query.save
			end
		else
			@shop_query = ShopQuery.new
		end
	end

	def news
		@cars = Car.select('`car_model`.`name` as model_name, `car_mark`.`name` as `mark_name`, `cars`.`photo1_file_name`, `cars`.`year`, `cars`.`price`, `cars`.`id` as `id`, `cars`.`status_new`')
		@cars = @cars.joins("JOIN `car_mark` ON `cars`.`mark`=`car_mark`.`id_car_mark`")
		@cars = @cars.joins("JOIN `car_model` ON `cars`.`model`=`car_model`.`id_car_model`")
		@cars = @cars.where("special = 1")
		@cars = @cars.limit(5)
		@news = News.all.order('id DESC')
		if params[:alias].blank?
			@content = @news[0].content
			@title = @news[0].title
		else
			news = News.where(alias: params[:alias])
			if !news.blank?
				@content = news[0].content
				@title = news[0].title
			else
				render '404'
			end
		end
	end

	def textpage
		if params[:slug].blank?
			redirect_to '/404'
			return
		end

		@page = Textpage.where(:slug => params[:slug]) rescue nil

		if @page.size == 0
			redirect_to '/404'
			return
		end

	end

	def map 
		
	end
end

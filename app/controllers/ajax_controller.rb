class AjaxController < ApplicationController

	def get_models
		render :json => Car::get_models(params[:id])
	end

end
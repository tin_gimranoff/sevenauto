class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  protect_from_forgery with: :null_session

  before_filter :construct


	 def authenticate_admin_user!
	  authenticate_user! 
	  unless current_user.admin?
	    flash[:alert] = "This area is restricted to administrators only."
	    redirect_to root_path 
	  end
	end
	 
	def current_admin_user
	  return nil if user_signed_in? && !current_user.admin?
	  current_user
	end

	private
		def construct
			@news_widget = News.order('id DESC').limit(3)
			@menu_items = Menu.where(:hide => 0, :parent => nil).order('ordernum ASC')
			if params[:request_rent]
				params.permit!
				@request_rent = RequestRent.new(params[:request_rent])
				if @request_rent.valid?
					@request_rent.save
					RequestRentMailer.send_rent_mail(@request_rent.name, @request_rent.phone, @request_rent.email, @request_rent.sq, @request_rent.info)
				end
			else
				@request_rent = RequestRent.new
			end
			page_info = Seo.where(url: request.path)[0]
			if !page_info.blank?
				@title = page_info.title
				@keywords = page_info.keywords
				@description = page_info.description
			end
		end

end

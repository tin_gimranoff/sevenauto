class HardwareController < ApplicationController

	def spare_parts
		@marks = CarMark.all
		@categories = ProductCategory.all
		if !params[:mark].blank? || !params[:category].blank?
			if params[:mark].blank? && !params[:category].blank?
				@spare_parts = SparePart.includes(:product_categories).where(:product_categories => {:id => params[:category]})
			end
			if !params[:mark].blank? && params[:category].blank?
				@spare_parts = SparePart.includes(:car_mark).where(:car_mark => {:id_car_mark => params[:mark]})
			end
			if !params[:mark].blank? && !params[:category].blank?
				@spare_parts_with_mark = SparePart.includes(:car_mark).includes(:product_categories).where(:product_categories => {:id => params[:category]})
				@spare_parts_with_mark = @spare_parts_with_mark.where(:car_mark => {:id_car_mark => params[:mark]})

				@spare_parts_without_mark = SparePart.includes(:car_mark).includes(:product_categories).where(:product_categories => {:id => params[:category]})
				@spare_parts_without_mark = @spare_parts_without_mark.where('car_mark.id_car_mark <> ?', params[:mark])

				@spare_parts = @spare_parts_with_mark | @spare_parts_without_mark
			end
		else
			@spare_parts = SparePart.all
		end
	end

	def spare_parts_details 
		@shop = SparePart.includes(:car_mark).find(params[:id]) rescue nil
		if @shop.nil?
			redirect_to '/404'
		end
	end

end
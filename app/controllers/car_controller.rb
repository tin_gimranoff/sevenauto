# encoding: utf-8
class CarController < ApplicationController

	def index
		@marks = CarMark.select('`car_mark`.`id_car_mark`, `car_mark`.`name`, count(`car_mark`.`id_car_mark`) as `count_cars`')
				 .joins('JOIN `cars` ON `cars`.`mark`=`car_mark`.`id_car_mark`')
				 .group('`car_mark`.`id_car_mark`')
				 .order('count(`car_mark`.`id_car_mark`) DESC')

		if params[:q]
			@cars = Car.search params[:q]
			@spare_parts = SparePart.search params[:q]
			@techcenters = TechCenter.search params[:q]
			render :index_search
			return
		end
		#render :json => params[:car].nil?
		@cars = Car.select('`car_model`.`name` as model_name, `car_mark`.`name` as `mark_name`, `cars`.`photo1_file_name`, `cars`.`year`, `cars`.`price`, `cars`.`id` as `id`, `cars`.`status_new`, `cars`.`mileage`, `cars`.`steering_wheel`, `cars`.`drive`, `cars`.`city`, `cars`.`power`, `cars`.`user_id`')
		@cars = @cars.joins("JOIN `car_mark` ON `cars`.`mark`=`car_mark`.`id_car_mark`")
		@cars = @cars.joins("JOIN `car_model` ON `cars`.`model`=`car_model`.`id_car_model`")
		#@cars.all
		if !params[:sort].blank? && !params[:sort_type].blank?
				sort = params[:sort]+' '+params[:sort_type]
				@cars = @cars.order(sort)
		else
			@cars = @cars.order('price DESC')
		end
		if !params[:car].nil?
			params[:car].each do |key, value|
				if value != '' and key != 'photo'
					key_param = key.split('_')
					if key_param.count == 1 
						@cars = @cars.where(key+" = ?", value)
					else
						if key_param[key_param.count-1] == 'to' || key_param[key_param.count-1] == 'from'
							if key_param[key_param.count-1] == 'to'
								key_param.pop
								key_param = key_param.join('_')
								@cars = @cars.where(key_param+" <= ?", value)
							else
								key_param.pop
								key_param = key_param.join('_')
								@cars = @cars.where(key_param+" >= ?", value)
							end
						else
							@cars = @cars.where(key+" = ?", value)
						end
					end
				else
					if key == 'photo' && value == 1
						@cars = @cars.where("photo1_file_name <> ''")
					end
				end
			end
		end
	end

	def send_ad
		if !user_signed_in?
			redirect_to '/users/sign_in'
			return
		end 

		@car = Car.new
		if params[:car]
			params.permit!
			@car = Car.new(params[:car])
			@car.status_new = 0 if params[:car][:status_new].blank?
			@car.status_access = 1
			if @car.valid?
				@car.user_id = current_user.id
				@car.save
				@status_success = 1
			end
		else
			@car = Car.new
		end
	end	

	def search
		
	end

	def advert
		if !params[:id]
			redirect_to '/404'
			return
		end
		@car = Car.select('cars.*, `car_mark`.`name` as `mark_name`, `car_model`.`name` as `model_name`')
				  .joins('JOIN `car_model` ON `cars`.`model`=`car_model`.`id_car_model`')
				  .joins('JOIN `car_mark` ON `cars`.`mark`=`car_mark`.`id_car_mark`')
				  .find(params[:id]) rescue nil
		if @car.nil?
			redirect_to '/404'
			return
		end

		@title = Array.new
		@title << @car.mark_name+' '+@car.model_name
		if @car.city.blank?
			city = User.find(@car.user_id).city
		else
			city = @car.city
		end
		@title << city
		@title = "Продам "+@title.join(' — ')

		@cars = Car.select('`car_model`.`name` as model_name, `car_mark`.`name` as `mark_name`, `cars`.`photo1_file_name`, `cars`.`year`, `cars`.`price`, `cars`.`id` as `id`, `cars`.`status_new`')
		@cars = @cars.joins("JOIN `car_mark` ON `cars`.`mark`=`car_mark`.`id_car_mark`")
		@cars = @cars.joins("JOIN `car_model` ON `cars`.`model`=`car_model`.`id_car_model`")
		@cars = @cars.where("`cars`.`steering_wheel` = ? AND `cars`.`transmission` = ? AND `cars`.`year` >= ? AND `cars`.`year` <= ? AND `cars`.`price` >= ? AND `cars`.`price` <= ? AND `cars`.`id` <> ?", @car.steering_wheel, @car.transmission, @car.year-3, @car.year+3, @car.price-100000, @car.price+100000, params[:id])
		@car.output_params.each do |p|
			if @car.attributes[p] == 1
				@cars = @cars.where("`cars`.`#{p}` = 1")
			end
		end
		#render :json => @car.output_params
	end
end
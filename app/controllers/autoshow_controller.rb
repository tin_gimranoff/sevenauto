class AutoshowController < ApplicationController

	def autoshows
		@autoshows = Autoshow.order('name DESC')
	end

	def autoshow_details 
		@shop = Autoshow.find(params[:id]) rescue nil
		if @shop.nil?
			redirect_to '/404'
		else
			@cars = Car.select('`car_model`.`name` as model_name, `car_mark`.`name` as `mark_name`, `cars`.`photo1_file_name`, `cars`.`year`, `cars`.`price`, `cars`.`id` as `id`, `cars`.`status_new`')
			@cars = @cars.joins("JOIN `car_mark` ON `cars`.`mark`=`car_mark`.`id_car_mark`")
			@cars = @cars.joins("JOIN `car_model` ON `cars`.`model`=`car_model`.`id_car_model`")
			@cars = @cars.where(user_id: @shop.user_id)
		end
	end

end
ThinkingSphinx::Index.define :spare_part, :with => :active_record do
  # fields
  indexes name, :sortable => true
  indexes description
  indexes phone
  indexes email
  indexes site
  indexes address
  indexes car_mark(:name), :as => 'mark_names', :separator => '/'
  indexes product_categories(:name), :as => 'category_names', :separator => '/'

  # attributes
  has created_at, updated_at
 #has car_mark(:name), :as => 'car_mark_names'

end
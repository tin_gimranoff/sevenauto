ThinkingSphinx::Index.define :car, :with => :active_record do
  # fields
  indexes car_mark.name, :as => :mark_name
  indexes car_model.name, :as => :model_name
  indexes price
  indexes year
  indexes mileage
  indexes drive
  indexes transmission
  indexes steering_wheel
  indexes displacement
  indexes power
  indexes body_type
  indexes color
  indexes city
  indexes user.city, :as => :user_city
  indexes description

  # attributes
  has mark, model, user_id, created_at, updated_at
end
ThinkingSphinx::Index.define :tech_center, :with => :active_record do
  # fields
  indexes name, :sortable => true
  indexes description
  indexes phone
  indexes email
  indexes site
  indexes address
  indexes work_categories(:name), :as => 'category_names', :separator => '/'

  # attributes
  has created_at, updated_at

end
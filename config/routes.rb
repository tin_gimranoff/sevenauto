Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  ActiveAdmin.routes(self)
  #devise_for :users
  devise_scope :user do
    match 'users/advert/:id' => 'registrations#advert', via: ['get', 'post', 'patch', 'delete']
  end
  devise_for :users, :controllers => { :registrations => 'registrations' }
  #resources :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'site#index', via: ['get', 'post']
  match 'news/(:alias)' => 'site#news', via: ['get', 'post']
  match 'map' => 'site#map', via: ['get', 'post']
  match 'car' => 'car#index', via: ['get', 'post']
  match 'car/advert/:id' => 'car#advert', via: ['get', 'post', 'patch']
  match 'car/:action' => 'car#:action', via: ['get', 'post', 'patch']
  match 'spare_parts' => 'hardware#spare_parts', via: ['get', 'post'] 
  match 'spare_parts/:id' => 'hardware#spare_parts_details', via: ['get', 'post'] 
  match 'techcenters' => 'techcenter#techcenters', via: ['get', 'post'] 
  match 'techcenter/:id' => 'techcenter#techcenter_details', via: ['get', 'post'] 
  match 'autoshows' => 'autoshow#autoshows', via: ['get', 'post'] 
  match 'autoshow/:id' => 'autoshow#autoshow_details', via: ['get', 'post'] 
  match ':slug' => 'site#textpage', via: ['get', 'post']

  post 'ajax/:action' => 'ajax#get_models'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
